import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class Service {

    public static List<String> getInfo(List<Base> objects) throws IllegalAccessException {
        List<String> res = new ArrayList<>(objects.size());
        for (Base obj : objects) {
            if(obj instanceof Derived){
                res.add(obj.getDescription() + ", " + ((Derived) obj).getAdditionalDescription());
            }
            else {
                res.add(obj.getDescription());
            }
        }
        return res;
    }

    public static boolean isFunctionalInterface(Class<?> inClass) {
        if (!inClass.isInterface())
            return false;

        Method[] methods = inClass.getDeclaredMethods();
        Method publicAbstract = null;

        for (Method interfaceMethod: methods) {
            int modifiers = interfaceMethod.getModifiers();
            if (Modifier.isPublic(modifiers) && Modifier.isAbstract(modifiers)) {
                if (publicAbstract == null){
                    publicAbstract = interfaceMethod;
                }
                else
                    return false;
            }
        }
        if (publicAbstract != null){
            return true;
        }
        else return false;
    }

    public static boolean isImplementsSerializable(Object object){
        Class<?>[] interfaces = object.getClass().getInterfaces();
        for (Class<?> myInteface : interfaces) {
            if(myInteface == Serializable.class);
                return true;
        }
        return false;
    }

    public static boolean isSerializable(Object object) {
        return object instanceof Serializable;
    }


    public static List<String> getCanonicalNamesWithStaticMethods(List<Class<?>> classes) {
        List<String> canonicalNames = new ArrayList<>();

        for (Class<?> isClass: classes) {
            Method[] methods = isClass.getMethods();

            for (Method method: methods) {
                if (Modifier.isStatic(method.getModifiers())) {
                    canonicalNames.add(isClass.getCanonicalName());
                    break;
                }
            }
        }

        return canonicalNames;
    }
}
