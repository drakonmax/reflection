import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class TestService {
    @Test
    void testGetInfo() throws IllegalAccessException {
        Base base0 = new Base("Описание");
        Base base1 = new Base("Другое описание");
        Base base2 = new Derived("Описание и", "Секрет");
        Derived derived0 = new Derived("Порождённое описание", "Свой секрет");
        Derived derived1 = new Derived("Много описаний", "Может хватит?");
        Derived derived2 = new Derived("Ну ладно, последнее", "Но это не точно");

        assertEquals(Arrays.asList("Описание", "Другое описание", "Описание и, Секрет"),
                        Service.getInfo(Arrays.asList(base0, base1, base2)));
        assertEquals(Arrays.asList("Порождённое описание, Свой секрет", "Много описаний, Может хватит?",
                "Ну ладно, последнее, Но это не точно"), Service.getInfo(Arrays.asList(derived0, derived1, derived2)));
    }


    @Test
    void testIsFunctionalInterface() {
        assertFalse(Service.isFunctionalInterface(Base.class));
        assertFalse(Service.isFunctionalInterface(Derived.class));
        assertFalse(Service.isFunctionalInterface(Serializable.class));
        assertTrue(Service.isFunctionalInterface(Supplier.class));
        assertTrue(Service.isFunctionalInterface(Consumer.class));
    }


    @Test
    void testIsImplementsSerializable() {
        assertTrue(Service.isImplementsSerializable(new Base("Снова описания")));
        assertFalse(Service.isImplementsSerializable(new Derived("Ну еще чуть","Совсем")));
        assertTrue(Service.isImplementsSerializable(666));
        assertFalse(Service.isImplementsSerializable(new Object()));
    }


    @Test
    void testIsSerializable() {
        assertTrue(Service.isSerializable(new Base("Больше описаний")));
        assertTrue(Service.isSerializable(new Derived("Хм","А теперь true")));
        assertTrue(Service.isSerializable(666));
        assertFalse(Service.isSerializable(new Object()));
    }


    @Test
    void testGetCanonicalNamesWithStaticMethods() {
        assertEquals(Arrays.asList("Service", "java.util.Arrays"),
                Service.getCanonicalNamesWithStaticMethods(Arrays.asList(Base.class, Service.class, Arrays.class,
                        Consumer.class, Supplier.class, TestService.class)));
    }
}
